//
// SearchPlate.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct SearchPlate: Codable {

    public var number: String

    public init(number: String) {
        self.number = number
    }


}
