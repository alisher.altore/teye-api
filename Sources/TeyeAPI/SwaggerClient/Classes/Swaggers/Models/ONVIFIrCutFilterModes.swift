//
// ONVIFIrCutFilterModes.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


/** An enumeration. */
public enum ONVIFIrCutFilterModes: String, Codable {
    case on = "ON"
    case off = "OFF"
    case auto = "AUTO"
}