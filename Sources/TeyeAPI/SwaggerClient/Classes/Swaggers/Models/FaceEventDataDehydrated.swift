//
// FaceEventDataDehydrated.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct FaceEventDataDehydrated: Codable {

    public var detection: EventDetection
    public var descriptor: FaceDescriptor?
    public var attributes: Data?

    public init(detection: EventDetection, descriptor: FaceDescriptor? = nil, attributes: Data? = nil) {
        self.detection = detection
        self.descriptor = descriptor
        self.attributes = attributes
    }


}
