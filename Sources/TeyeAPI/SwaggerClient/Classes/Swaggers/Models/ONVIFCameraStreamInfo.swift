//
// ONVIFCameraStreamInfo.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ONVIFCameraStreamInfo: Codable {

    public var name: String
    public var link: String
    public var currentEncoder: ONVIFSupportedEncoder
    public var currentResolution: ONVIFSupportedResolution
    public var currentFramerate: Int
    public var currentBitrate: Int
    public var currentGovLength: Int?
    public var supportedEncoders: [ONVIFCameraSupportedStreamEncoder]

    public init(name: String, link: String, currentEncoder: ONVIFSupportedEncoder, currentResolution: ONVIFSupportedResolution, currentFramerate: Int, currentBitrate: Int, currentGovLength: Int? = nil, supportedEncoders: [ONVIFCameraSupportedStreamEncoder]) {
        self.name = name
        self.link = link
        self.currentEncoder = currentEncoder
        self.currentResolution = currentResolution
        self.currentFramerate = currentFramerate
        self.currentBitrate = currentBitrate
        self.currentGovLength = currentGovLength
        self.supportedEncoders = supportedEncoders
    }

    public enum CodingKeys: String, CodingKey { 
        case name
        case link
        case currentEncoder = "current_encoder"
        case currentResolution = "current_resolution"
        case currentFramerate = "current_framerate"
        case currentBitrate = "current_bitrate"
        case currentGovLength = "current_gov_length"
        case supportedEncoders = "supported_encoders"
    }

}
