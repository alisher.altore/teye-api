//
// MotionEventList.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct MotionEventList: Codable {

    public var events: [MotionEvent]

    public init(events: [MotionEvent]) {
        self.events = events
    }


}
