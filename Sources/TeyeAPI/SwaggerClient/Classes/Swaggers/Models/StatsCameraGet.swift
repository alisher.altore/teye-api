//
// StatsCameraGet.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct StatsCameraGet: Codable {

    public var totalCount: Int
    public var activeCount: Int

    public init(totalCount: Int, activeCount: Int) {
        self.totalCount = totalCount
        self.activeCount = activeCount
    }

    public enum CodingKeys: String, CodingKey { 
        case totalCount = "total_count"
        case activeCount = "active_count"
    }

}
