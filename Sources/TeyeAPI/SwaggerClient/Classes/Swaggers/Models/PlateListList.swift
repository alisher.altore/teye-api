//
// PlateListList.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct PlateListList: Codable {

    public var platelists: [PlateListGet]

    public init(platelists: [PlateListGet]) {
        self.platelists = platelists
    }


}
