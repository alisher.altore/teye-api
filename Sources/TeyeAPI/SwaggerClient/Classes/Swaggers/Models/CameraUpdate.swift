//
// CameraUpdate.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct CameraUpdate: Codable {

    public var name: String
    public var latitude: Decimal
    public var longitude: Decimal
    public var source: Data
    public var videoAnalytics: Data?
    public var videoStorage: Data?
    public var videoStreaming: Data?
    public var ptz: Data?
    public var placeId: Int
    public var onvifHost: String?
    public var onvifPort: Int?
    public var onvifLogin: String?
    public var onvifPassword: String?

    public init(name: String, latitude: Decimal, longitude: Decimal, source: Data, videoAnalytics: Data? = nil, videoStorage: Data? = nil, videoStreaming: Data? = nil, ptz: Data? = nil, placeId: Int, onvifHost: String? = nil, onvifPort: Int? = nil, onvifLogin: String? = nil, onvifPassword: String? = nil) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.source = source
        self.videoAnalytics = videoAnalytics
        self.videoStorage = videoStorage
        self.videoStreaming = videoStreaming
        self.ptz = ptz
        self.placeId = placeId
        self.onvifHost = onvifHost
        self.onvifPort = onvifPort
        self.onvifLogin = onvifLogin
        self.onvifPassword = onvifPassword
    }

    public enum CodingKeys: String, CodingKey { 
        case name
        case latitude
        case longitude
        case source
        case videoAnalytics = "video_analytics"
        case videoStorage = "video_storage"
        case videoStreaming = "video_streaming"
        case ptz
        case placeId = "place_id"
        case onvifHost = "onvif_host"
        case onvifPort = "onvif_port"
        case onvifLogin = "onvif_login"
        case onvifPassword = "onvif_password"
    }

}
