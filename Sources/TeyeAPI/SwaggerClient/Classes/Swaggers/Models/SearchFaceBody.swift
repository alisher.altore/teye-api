//
// SearchFaceBody.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct SearchFaceBody: Codable {

    public var descriptor: SearchFaceDescriptor
    public var threshold: Decimal

    public init(descriptor: SearchFaceDescriptor, threshold: Decimal) {
        self.descriptor = descriptor
        self.threshold = threshold
    }


}
