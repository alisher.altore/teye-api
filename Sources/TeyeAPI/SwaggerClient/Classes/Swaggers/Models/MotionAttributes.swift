//
// MotionAttributes.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct MotionAttributes: Codable {

    public var objClass: MotionEventObjectClass

    public init(objClass: MotionEventObjectClass) {
        self.objClass = objClass
    }

    public enum CodingKeys: String, CodingKey { 
        case objClass = "obj_class"
    }

}
