//
// ONVIFCameraSettingsRange.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ONVIFCameraSettingsRange: Codable {

    public var minVal: Decimal
    public var maxVal: Decimal

    public init(minVal: Decimal, maxVal: Decimal) {
        self.minVal = minVal
        self.maxVal = maxVal
    }

    public enum CodingKeys: String, CodingKey { 
        case minVal = "min_val"
        case maxVal = "max_val"
    }

}
