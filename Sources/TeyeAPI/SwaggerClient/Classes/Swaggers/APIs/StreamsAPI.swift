//
// StreamsAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire


open class StreamsAPI {
    /**
     Get stream information by provided ID

     - parameter streamId: (path)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func getStream(streamId: UUID, completion: @escaping ((_ data: StreamGet?,_ error: Error?) -> Void)) {
        getStreamWithRequestBuilder(streamId: streamId).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     Get stream information by provided ID
     - GET /api/v1/streams/{stream_id}
     - 

     - OAuth:
       - type: oauth2
       - name: OAuth2PasswordBearer
     - examples: [{contentType=application/json, example={
  "streamserver_id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
  "streamserver_host" : "streamserver_host",
  "status" : "status",
  "ts" : 0
}}]
     - parameter streamId: (path)  

     - returns: RequestBuilder<StreamGet> 
     */
    open class func getStreamWithRequestBuilder(streamId: UUID) -> RequestBuilder<StreamGet> {
        var path = "/api/v1/streams/{stream_id}"
        let streamIdPreEscape = "\(streamId)"
        let streamIdPostEscape = streamIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{stream_id}", with: streamIdPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        let url = URLComponents(string: URLString)


        let requestBuilder: RequestBuilder<StreamGet>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
    /**
     Get stream information with provided ID

     - parameter streamId: (path)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func getStreamInfo(streamId: UUID, completion: @escaping ((_ data: StreamInfoGet?,_ error: Error?) -> Void)) {
        getStreamInfoWithRequestBuilder(streamId: streamId).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     Get stream information with provided ID
     - GET /api/v1/streams/{stream_id}/info
     - 

     - OAuth:
       - type: oauth2
       - name: OAuth2PasswordBearer
     - examples: [{contentType=application/json, example={
  "bit_rate" : 6,
  "codec" : "codec",
  "frame_width" : 1,
  "frame_height" : 5,
  "frame_rate" : 0
}}]
     - parameter streamId: (path)  

     - returns: RequestBuilder<StreamInfoGet> 
     */
    open class func getStreamInfoWithRequestBuilder(streamId: UUID) -> RequestBuilder<StreamInfoGet> {
        var path = "/api/v1/streams/{stream_id}/info"
        let streamIdPreEscape = "\(streamId)"
        let streamIdPostEscape = streamIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{stream_id}", with: streamIdPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        let url = URLComponents(string: URLString)


        let requestBuilder: RequestBuilder<StreamInfoGet>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
}
